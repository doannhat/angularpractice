import { Component, OnInit } from '@angular/core';
import { DataBindingService } from '../data-binding.service';

@Component({
  selector: 'app-data-binder',
  templateUrl: './data-binder.component.html',
  styleUrls: ['./data-binder.component.css']
})
export class DataBinderComponent implements OnInit {
  private dbs: DataBindingService;
  constructor(databindingservice: DataBindingService) {
    this.dbs = databindingservice;
  };
  
  ngOnInit() {
    this.dbs.logInput(event);
  }
}
