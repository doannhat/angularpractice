import { TestBed } from '@angular/core/testing';

import { FormsPracticeService } from './forms-practice.service';

describe('FormsPracticeService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: FormsPracticeService = TestBed.get(FormsPracticeService);
    expect(service).toBeTruthy();
  });
});
