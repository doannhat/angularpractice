import { TestBed } from '@angular/core/testing';

import { DirectiveServiceService } from './directive-service.service';

describe('DirectiveServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: DirectiveServiceService = TestBed.get(DirectiveServiceService);
    expect(service).toBeTruthy();
  });
});
