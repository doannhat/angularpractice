import { Component, OnInit } from '@angular/core';
import { FormsPracticeService } from '../forms-practice.service';
@Component({
  selector: 'app-forms',
  templateUrl: './forms.component.html',
  styleUrls: ['./forms.component.css']
})
export class FormsComponent implements OnInit {
  private text: string;
  private username: string;
  private password: string;
  private confirm: string;
  private result: boolean;
  constructor(formsService: FormsPracticeService) {
    this.text = formsService.text;
    this.username = formsService.username;
    this.password = formsService.password;
    this.confirm = formsService.confirm;
    this.result = formsService.result;
  }
  sendRequest(form) {
    const {username, password, confirm} = form.value;
    // console.log(`${username}\n${password}\n${confirm}`);
    if (!username && !password && !confirm) {
      this.text = 'You must not leave either field blank.';
      this.result = false;
    } else if (!this.verifyEmail(username)) {
      this.text = 'Email is invalid';
      this.result = false;
    } else if (password !== confirm) {
      this.text = 'Password not match.';
      this.result = false;
    } else if (!this.verifyPassword(password)) {
      this.text = 'Password is invalid.';
      this.result = false;
    } else {
      this.text = 'All good.'
      this.result = true;
    }
  }
  hasUppercase(str) {
    return /[A-Z]/.test(str);
  }
  hasLowerCase(str) {
    return /[a-z]/.test(str);
  }
  hasNumber(str) {
    return /\d/.test(str);
  }
  verifyPassword(password) {
    return (password.length > 5 && this.hasNumber(password) &&
        this.hasLowerCase(password) && this.hasUppercase(password));
  }
  verifyEmail(username) {
    return (username.includes('@') && username.charAt(0) !== '@' && username.charAt(username.length) !== '@');
  }
  ngOnInit() {
  }

}
