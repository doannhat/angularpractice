import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DataBindingService {
  textFormat = '';

  constructor() { }

  logInput(event) {
    // console.log(event.target);
    const splited = event.target.value.split(' ');
    console.log(splited);
    for (let i in splited) {
      if (Number.parseInt(i) == 0 || splited[i] !== 'of' && splited[i] !== 'the') {
        const j = splited[i].charAt(0).toUpperCase();
        splited[i] = j + splited[i].substr(1).toLowerCase();
      }
    }
    // console.log(splited.join(' '));
    this.textFormat = splited.join(' ');
    return this.textFormat;
  }
}
