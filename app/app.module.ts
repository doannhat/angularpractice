import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { AppComponent } from './app.component';
import { DataBinderComponent } from './data-binder/data-binder.component';
import { DirectiveComponent } from './directive/directive.component';
import { FormsComponent } from './forms/forms.component';

import { DataBindingService } from './data-binding.service';
import { DirectiveServiceService } from './directive-service.service';
import { FormsPracticeService } from './forms-practice.service';

@NgModule({
  declarations: [
    AppComponent,
    DataBinderComponent,
    DirectiveComponent,
    FormsComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule
  ],
  providers: [
    DataBindingService,
    DirectiveServiceService,
    FormsPracticeService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
