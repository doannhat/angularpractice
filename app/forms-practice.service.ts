import { Injectable } from '@angular/core';
import {  HttpClient } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class FormsPracticeService {
  text: string;
  username: string;
  password: string;
  confirm: string;
  result: boolean;

  constructor(private http: HttpClient) { 
    
  }
}
