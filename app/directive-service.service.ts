import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class DirectiveServiceService {
  constructor() { 
    
  };
  arr = [
    {
      lessons: ['lesson 1', 'lesson 2', 'lesson 3'],
      visibility: false
    },
    {
      lessons: ['lesson A', 'lesson B'],
      visibility: false
    }
  ];

}
