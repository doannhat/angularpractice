import { Component, OnInit } from '@angular/core';
import { DirectiveServiceService } from '../directive-service.service';

@Component({
  selector: 'app-directive',
  templateUrl: './directive.component.html',
  styleUrls: ['./directive.component.css']
})
export class DirectiveComponent implements OnInit {
  course1: string[];
  visibility1: boolean;
  course2: string[];
  visibility2: boolean;
  constructor(directiveService: DirectiveServiceService) {
    this.course1 = directiveService.arr[0].lessons;
    this.visibility1 = directiveService.arr[0].visibility;
    this.course2 = directiveService.arr[1].lessons;
    this.visibility2 = directiveService.arr[1].visibility;
  }
  showItems1(){
    this.visibility1 = true;
  }
  hideItems1(){
    this.visibility1 = false;
  }
  showItems2(){
    this.visibility2 = true;
  }
  hideItems2(){
    this.visibility2 = false;
  }
  ngOnInit() {
  }

}
